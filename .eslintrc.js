module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  parserOptions: {
    ecmaVersion: 8,
  },
  extends: ['eslint:recommended'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  rules: {
    'no-extra-semi': 'off',
    'no-undef': ['error'],
    eqeqeq: ['warn', 'smart'],
    'no-unused-vars': 'warn',
    'no-prototype-builtins': 'off',
    'object-shorthand': ['warn', 'never'],
    'prefer-const': 'warn',
  },
}
