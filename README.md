## Install

1. Greasyfork-s guide on installing userscripts: https://greasyfork.org/en/help/installing-user-scripts
2. Run `./make.sh` with a `DOMAIN` env variable set (`DOMAIN=some.domain`) to replace urls inside the
   script
3. Open stable.user.js in a browser. The browser addon should recognize it, and ask if you want to
   install it

## Server

The server for this userscript is available here: https://gitlab.com/MrFry/mrfrys-node-server

If used as a git submodule in the server repo, then the `make.sh` script will use the domain
specified in a file there

## License:
GPLv3
